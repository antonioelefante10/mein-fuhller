//
//  GameScene.swift
//  Mein Fuhller
//
//  Created by antonio elefante on 05/12/2018.
//  Copyright © 2018 antonio elefante. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit
import GoogleMobileAds

class GameScene: SKScene , SKPhysicsContactDelegate, UIGestureRecognizerDelegate {
    
    private var gelati: [SKSpriteNode] = []
    private var timeSpeed: Float = 3
    private var myCamera = SKCameraNode()
    private var points = 0
    private var conoGelato: SKSpriteNode = SKSpriteNode(imageNamed: "Cono"+String(DesignElements.conoDesign))
    private var touched: Bool = false
    var gnak = SKAction.playSoundFileNamed("squirt.wav", waitForCompletion: false)
    var woohoo = SKAction.playSoundFileNamed("woo.mp3", waitForCompletion: false)
    var splink = SKAction.playSoundFileNamed("fairydust.mp3", waitForCompletion: false)
    private var arrayOfTouches: [CGPoint] = []
    private let musictheme = SKAudioNode (fileNamed: "HollyJolly.mp3")
    private var cialda1: SKSpriteNode!
    private var cialda2: SKSpriteNode!
    private var cialda3: SKSpriteNode!
    private var cialda4: SKSpriteNode!
    private var table: SKSpriteNode!
    private var punteggio: SKLabelNode!
    private var spunteggio: SKLabelNode!
    private var menu: SKSpriteNode!
    private var retryNow: SKSpriteNode!
    private var neverMissCount:Int = 0
    private var touchLocation: CGPoint!
    
    private var tendina = SKSpriteNode (imageNamed: "Tenda2")
    func posizionaTendina(){
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: -100)
        tendina.zPosition = 2000
        self.addChild(tendina)
        tendaCheSale()
    }
    func tendaCheSale()
    {
        let up = SKAction.move(to: CGPoint(x: 0, y: (self.view?.frame.height)! ), duration: 1)
        tendina.run(up)
        let wait = SKAction.wait(forDuration: 3)
        
        self.run(SKAction.sequence([wait,SKAction.run {
            self.tendina.removeFromParent()
        }]))
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    var action: SKAction!
    @objc func longPress(sender: UILongPressGestureRecognizer) {
        
        if sender.state == .began {

            circleLayer.removeAllAnimations()
            circleLayer.removeFromSuperlayer()
            
            touchLocation = sender.location(in: self.view)
            let node = self.atPoint(touchLocation)
            if node.name == "1" || node.name == "2" || node.name == "3" || node.name == "4" {
                return
            }
            addCircleView(position: touchLocation)
            action = SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run {

                self.circleLayer.removeFromSuperlayer()
                self.circleLayer.removeAllAnimations()
                let scene = MainMenu(size: self.size)
                
                self.changeScene(scene: scene)
                }])
            self.run(action, withKey: "pauseAction")
            
        }
        if sender.state == .ended {
           
                circleLayer.removeFromSuperlayer()
                circleLayer.removeAllAnimations()
            
            
            
                
                
                self.removeAction(forKey: "pauseAction")

        }
        
        
    }
    func tendaCheScende()
    {
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: self.view!.frame.height + 10)
        tendina.zPosition = 2000
        self.addChild(tendina)
        let down = SKAction.move(to: CGPoint(x: 0, y: -100 ), duration: 1)
        tendina.run(down)
    }
    
    func addCircleView(position: CGPoint) {
        
        guard let view = self.view else {
            return
        }
        
        let circlePath = UIBezierPath(arcCenter: position, radius: (frame.size.width)/10, startAngle: 0.0, endAngle: CGFloat(.pi * 2.0), clockwise: true)
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = UIColor.brown.cgColor
        circleLayer.lineWidth = 5.0;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        // Add the circleLayer to the view's layer's sublayers
        view.layer.addSublayer(circleLayer)
        
        
        animateCircle(duration: 3)
        view.layer.addSublayer(circleLayer)
    }
    
    
    func animateCircle(duration: TimeInterval) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override func didMove(to view: SKView) {
       
        
        if !isReloaded {
            posizionaTendina()
        }
        isReloaded = false
        
        let pressed:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(sender:)))
        pressed.delegate = self
        pressed.minimumPressDuration = 0.5
        view.addGestureRecognizer(pressed)
        
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: UDKeys.gamePlayedKey)+1, forKey: UDKeys.gamePlayedKey)
        
        self.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),SKAction.run {
            self.evaluateMedals()
            }]))
        
        self.backgroundColor = DesignElements.backgroundColor
        let shadow = NSShadow()
        shadow.shadowColor = UIColor.gray
        shadow.shadowOffset = CGSize (width: 2, height: -2)
        self.addChild(musictheme)
        
        
        table = SKSpriteNode (imageNamed: "Tavolo2")
        table.anchorPoint = CGPoint(x: 0, y: 0)
        table.position = CGPoint(x: 0, y: -view.frame.height*0.1)
        table.size = CGSize (width: view.frame.width , height: view.frame.height*0.2)
        table.zPosition = -2
        self.addChild(table)
        setUpLose()
        myCamera.xScale = 1
        myCamera.yScale = 1
        myCamera.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        self.camera = myCamera
        
        cialda1 = SKSpriteNode(imageNamed: "Cialda"+String(DesignElements.cialdaDesign)+"-1")
        cialda1.name = "1"
        cialda1.anchorPoint = CGPoint(x: 0, y: 0)
        cialda1.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.3)
        cialda1.position = CGPoint(x: -view.frame.width/2 - view.frame.width*0.1 * 2/3, y: -view.frame.height/2 - view.frame.width*0.1 * 2/3)
        camera!.addChild(cialda1)
        
        
        cialda2 = SKSpriteNode(imageNamed: "Cialda"+String(DesignElements.cialdaDesign)+"-2")
        cialda2.name = "2"
        cialda2.anchorPoint = CGPoint(x: 0, y: 1)
        cialda2.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.3)
        cialda2.position = CGPoint (x: -view.frame.width/2 - view.frame.width*0.1*2/3, y: view.frame.height/2+view.frame.width*0.1*2/3)
        camera!.addChild(cialda2)
        
        cialda3 = SKSpriteNode(imageNamed: "Cialda"+String(DesignElements.cialdaDesign)+"-3")
        cialda3.name = "3"
        cialda3.anchorPoint = CGPoint(x: 1, y: 1)
        cialda3.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.3)
        cialda3.position = CGPoint (x: view.frame.width/2 + view.frame.width*0.1*2/3, y: view.frame.height/2 + view.frame.width * 0.1 * 2/3)
        camera!.addChild(cialda3)
        
        cialda4 = SKSpriteNode(imageNamed: "Cialda"+String(DesignElements.cialdaDesign)+"-4")
        cialda4.name = "4"
        cialda4.anchorPoint = CGPoint(x: 1, y: 0)
        cialda4.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.3)
        cialda4.position = CGPoint (x: view.frame.width/2 + view.frame.width * 0.1 * 2/3, y: -view.frame.height/2 - view.frame.width * 0.1*2/3)
        camera!.addChild(cialda4)
        
        
        
        
        
        var gelatoRosso: SKSpriteNode
        gelatoRosso = SKSpriteNode(imageNamed: "Gelato"+String(DesignElements.gelatoDesign))
        gelatoRosso.position = CGPoint(x: view.frame.maxX, y: view.frame.height * 0.5 + myCamera.position.y - view.frame.width / 7 * 2)
        gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)
        let actionMoveBackward = SKAction.move(to: CGPoint(x: view.frame.minX+gelatoRosso.size.width, y: gelatoRosso.position.y),
                                               duration: TimeInterval(timeSpeed))
        let actionMoveForward = SKAction.move(to: CGPoint(x: view.frame.maxX-gelatoRosso.size.width, y: gelatoRosso.position.y), duration: TimeInterval(timeSpeed))
        gelatoRosso.run(SKAction.repeatForever(SKAction.sequence([actionMoveBackward, actionMoveForward])))
        gelati.append(gelatoRosso)
        self.addChild(gelatoRosso)
        
        conoGelato.position = CGPoint(x: view.frame.midX, y: view.frame.height*0.08)
        conoGelato.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 5)
        let conoGelatoSize = CGSize(width: conoGelato.size.width, height: conoGelato.size.height)
        conoGelato.physicsBody = SKPhysicsBody(rectangleOf: conoGelatoSize, center: CGPoint(x: 0, y: -conoGelato.size.height/3))
        conoGelato.physicsBody?.categoryBitMask = PhysicsCategory.cono
        conoGelato.physicsBody?.isDynamic = false
        conoGelato.physicsBody?.friction = 1
        conoGelato.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        conoGelato.physicsBody?.collisionBitMask = PhysicsCategory.gelato
        conoGelato.physicsBody?.affectedByGravity = false
        conoGelato.zPosition = -1
        conoGelato.physicsBody?.restitution = 0.0
        
        
        setUpWall()
        
        
        self.addChild(conoGelato)
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -3)
        self.physicsWorld.contactDelegate = self
        self.addChild(myCamera)
        
        
        
        punteggio = makeTextLabel(text: "Score: 0", size: view.frame.width/15, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.95))
        var punteggioPosition: CGPoint
        if hasTopNotch {
            punteggioPosition = CGPoint(x: 0, y: view.frame.height/2 - punteggio.frame.height*5/2)
        }
        else {
            punteggioPosition = CGPoint(x: 0, y: view.frame.height/2 - punteggio.frame.height*3/2)
        }
        punteggio.position = punteggioPosition
        self.myCamera.addChild(punteggio)
        
        
        
    }
    
    func evaluateMedals(){
        if UserDefaults.standard.bool(forKey: UDKeys.granellaKey) == false {
            UserDefaults.standard.set(true, forKey: UDKeys.granellaKey)
            presentMedalUnlockPopup(icon: "MedagliaGelato", title: MedalsLabel.granellaTitle, message: MedalsLabel.granellaMessage)
        }
        if UserDefaults.standard.bool(forKey: UDKeys.impilaKey) == false {
            if UserDefaults.standard.integer(forKey: UDKeys.gamePlayedKey) >= 10 {
                UserDefaults.standard.set(true, forKey: UDKeys.impilaKey)
                presentMedalUnlockPopup(icon: "medagliaImpila",title: MedalsLabel.impilaTitle, message: MedalsLabel.impilaMessage)
            }
        }
        if UserDefaults.standard.bool(forKey: UDKeys.carrettinoKey) == false {
            if UserDefaults.standard.integer(forKey: UDKeys.gamePlayedKey) >= 20 {
                UserDefaults.standard.set(true, forKey: UDKeys.carrettinoKey)
                presentMedalUnlockPopup(icon: "medagliaCarrettino", title: MedalsLabel.carrettinoTitle, message: MedalsLabel.carrettinoMessage)
            }
        }
    }
    
    func evaluateNeverMiss(){
        if UserDefaults.standard.bool(forKey: UDKeys.collaborazioneKey) == false {
            
            UserDefaults.standard.set(true, forKey: UDKeys.collaborazioneKey)
            presentMedalUnlockPopup(icon: "medagliaCollaborazione", title: MedalsLabel.collaborazioneTitle, message: MedalsLabel.collaborazioneMessage)
            
        }
    }
    
    func evaluatePoints(){
        if UserDefaults.standard.bool(forKey: UDKeys.cannellaKey) == false {
            if points >= 5 {
                UserDefaults.standard.set(true, forKey: UDKeys.cannellaKey)
                presentMedalUnlockPopup(icon: "medagliaCannella", title: MedalsLabel.cannellaTitle, message: MedalsLabel.cannellaMessage)
            }
        }
        if UserDefaults.standard.bool(forKey: UDKeys.cacchinaKey) == false {
            if points >= 25 {
                UserDefaults.standard.set(true, forKey: UDKeys.cacchinaKey)
                presentMedalUnlockPopup(icon: "medagliaCacchina", title: MedalsLabel.cacchinaTitle, message: MedalsLabel.cacchinaMessage)
            }
        }
        if UserDefaults.standard.bool(forKey: UDKeys.oroKey) == false {
            if points >= 40 {
                UserDefaults.standard.set(true, forKey: UDKeys.oroKey)
                presentMedalUnlockPopup(icon: "medagliaOro",title: MedalsLabel.oroTitle, message: MedalsLabel.oroMessage)
            }
        }
        if UserDefaults.standard.bool(forKey: UDKeys.kingKey) == false {
            if points >= 75 {
                UserDefaults.standard.set(true, forKey: UDKeys.kingKey)
                presentMedalUnlockPopup(icon: "medagliaKing", title: MedalsLabel.kingTitle, message: MedalsLabel.kingMessage)
            }
        }
        if UserDefaults.standard.bool(forKey: UDKeys.finaleKey) == false {
            if points >= 100{
                UserDefaults.standard.set(true, forKey: UDKeys.finaleKey)
                presentMedalUnlockPopup(icon: "medagliaFinale",title: MedalsLabel.finaleTitle, message: MedalsLabel.finaleMessage)
            }
        }
    }
    
    
    
    func presentMedalUnlockPopup(icon: String, title: String, message: String){
        guard let view = self.view else {
            return
        }
        
        self.run(self.splink)
        
        let medalTitleText = makeTextLabel(text: title, size: view.frame.width/14, position: CGPoint(x:myCamera.frame.midX,y:myCamera.frame.minY))
        medalTitleText.zPosition = CGFloat(points + 100)
        var medalUnlockedPosition = medalTitleText.position
        medalUnlockedPosition.y = medalUnlockedPosition.y - medalTitleText.frame.height
        let medalUnlockedText = makeTextLabel(text: "Unlocked", size: view.frame.width/14, position: medalUnlockedPosition )
        self.addChild(medalTitleText)
        self.addChild(medalUnlockedText)
        let path1 = CGMutablePath()
        path1.move(to: CGPoint(x: myCamera.position.x - view.frame.width/2, y: myCamera.position.y - view.frame.height/2))
        path1.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 - view.frame.width*0.1, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.1))
        path1.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 - view.frame.width*0.2, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.15))
        path1.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 - view.frame.width*0.3, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.1))
        path1.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 - view.frame.width*0.4, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.05))
        
        let followAction1 = SKAction.follow(path1, speed: 500 * view.frame.height/1366)
        
        let star1 = SKSpriteNode(imageNamed: "Star")
        star1.zPosition = CGFloat(points + 100)
        star1.size = CGSize(width: view.frame.width*0.1, height: view.frame.width*0.1)
        star1.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        self.addChild(star1)
        star1.run(SKAction.sequence([followAction1,SKAction.run({
            star1.removeFromParent()
        })]))
        
        let path2 = CGMutablePath()
        
        path2.move(to: CGPoint(x: myCamera.position.x - view.frame.width/2, y: myCamera.position.y - view.frame.height/2))
        
        path2.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 + view.frame.width*0.1, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.1))
        path2.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 + view.frame.width*0.2, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.15))
        path2.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 + view.frame.width*0.3, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.1))
        path2.addLine(to: CGPoint(x: myCamera.position.x - view.frame.width/2 + view.frame.width*0.4, y: myCamera.position.y - view.frame.height/2 + view.frame.height*0.05))
        
        let followAction2 = SKAction.follow(path2, speed: 500 * view.frame.height/1366)
        
        let star2 = SKSpriteNode(imageNamed: "Star")
        star2.zPosition = CGFloat(points + 100)
        star2.size = CGSize(width: view.frame.width*0.1, height: view.frame.width*0.1)
        star2.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        self.addChild(star2)
        star2.run(SKAction.sequence([followAction2,SKAction.run({
            star2.removeFromParent()
            self.run(SKAction.sequence([SKAction.wait(forDuration: 0.2),SKAction.run {
                medalTitleText.removeFromParent()
                medalUnlockedText.removeFromParent()
                }]))
        })]))
        
        
        
        
    }
    
    func setUpWall(){
        guard let myView = self.view else {
            return
        }

        let rightWall = SKSpriteNode(imageNamed: "Cane")
        rightWall.position = CGPoint(x: myView.frame.width/2 - rightWall.size.width/2, y: 0)
        rightWall.zPosition = -10
//        rightWall.size = CGSize(width: conoGelato.frame.width*0.1, height: myView.frame.height+5*conoGelato.size.height)
//
        let leftWall = SKSpriteNode(imageNamed: "Cane")
        leftWall.position = CGPoint(x: -myView.frame.width/2 + leftWall.size.width/2, y: 0)
        leftWall.zPosition = -10
//        leftWall.size = CGSize(width: conoGelato.frame.width*0.1, height: myView.frame.height+5*conoGelato.size.height)


        myCamera.addChild(rightWall)
        myCamera.addChild(leftWall)

    }
    
    
    func setUpLose(){
        let downWall = SKSpriteNode()
        guard let myView = self.view else {
            return
        }
        downWall.color = .red
        downWall.position = CGPoint(x: myView.frame.midX, y: myView.frame.minY-myView.frame.height*0.1)
        downWall.size = CGSize(width: myView.frame.width, height: myView.frame.height*0.1)
        downWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width, height: myView.frame.height*0.1))
        downWall.physicsBody?.isDynamic = false
        downWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        downWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        downWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        let rightWall = SKSpriteNode()
        
        rightWall.color = .red
        rightWall.anchorPoint = CGPoint(x: 0, y:0)
        //        rightWall.position = CGPoint(x: myView.frame.width/2+myView.frame.width*0.1, y: -myView.frame.height/2)
        //        rightWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        rightWall.position = CGPoint(x: myView.frame.maxX+myView.frame.width*0.1, y: myView.frame.minY)
        rightWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        rightWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000))
        rightWall.physicsBody?.isDynamic = false
        rightWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        rightWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        rightWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        let leftWall = SKSpriteNode()
        
        leftWall.color = .red
        //        leftWall.anchorPoint = CGPoint(x:0 , y: 0)
        //        leftWall.position = CGPoint(x: -myView.frame.width/2-myView.frame.width*0.1, y: -myView.frame.height/2)
        //        leftWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        leftWall.position = CGPoint(x: myView.frame.minX-myView.frame.width*0.1, y: myView.frame.minY)
        leftWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        
        leftWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000))
        leftWall.physicsBody?.isDynamic = false
        leftWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        leftWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        leftWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        
        self.addChild(downWall)
        self.addChild(rightWall)
        self.addChild(leftWall)
    }
    
    func touchDown(atPoint pos : CGPoint) {
        if !touched {
            touched = true
            gelati[gelati.count-1].removeAllActions()
            let gelatoSize = CGSize(width: gelati[gelati.count-1].size.width, height: gelati[gelati.count-1].size.height - gelati[gelati.count-1].size.height/3)
            gelati[gelati.count-1].physicsBody = SKPhysicsBody(rectangleOf: gelatoSize, center: CGPoint(x: 0, y: -gelati[gelati.count-1].size.height/3))
            gelati[gelati.count-1].physicsBody?.isDynamic = true
            gelati[gelati.count-1].physicsBody?.friction = 1
            gelati[gelati.count-1].physicsBody?.allowsRotation = true
            gelati[gelati.count-1].physicsBody?.categoryBitMask = PhysicsCategory.gelato
            gelati[gelati.count-1].physicsBody?.contactTestBitMask = PhysicsCategory.gelato | PhysicsCategory.cono
            gelati[gelati.count-1].physicsBody?.collisionBitMask = PhysicsCategory.all
            gelati[gelati.count-1].physicsBody?.affectedByGravity = true
            gelati[gelati.count-1].physicsBody?.restitution = 0.0
            
        }
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            
            let location = t.location(in: self)
            if menu != nil
            {
                if menu.contains(location)
                {
                    let scene = MainMenu(size: self.size)
                    
                    changeScene(scene: scene)
                }
                
            }
            if retryNow != nil {
                if retryNow.contains(location) {
                    isReloaded = true
                    let scene = GameScene(size: self.size) // Whichever scene you want to restart (and are in)
                    let animation = SKTransition.fade(with: UIColor(red: 249/255, green: 201/255, blue: 123/255, alpha: 1.0), duration: 1) // ...Add transition if you like
                    let gameViewController = self.view?.window?.rootViewController as! GameViewController
                    gameViewController.removeBannerFromScene()
                    self.view?.presentScene(scene, transition: animation)
                }
            }
            arrayOfTouches.append(t.location(in: myCamera))
        }
        if arrayOfTouches.count >= 4 {
            funzioneMagicaCheFaTocchiNegliAngoli()
        }
        
        
        let wait = SKAction.wait(forDuration: 0.35)
        
        
        self.run(wait) {
            self.arrayOfTouches.removeAll()
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
    }
    
    func funzioneMagicaCheFaTocchiNegliAngoli(){
        var arrayNode: [SKNode] = []
        for i in 0...3 {
            let node: SKNode = myCamera.atPoint(arrayOfTouches[i])
            arrayNode.append(node)
        }
        var trovato = false
        var i = 0
        while !trovato && i<=2 {
            for j in i+1...3 {
                if arrayNode[i].name == arrayNode[j].name {
                    trovato = true
                }
            }
            i = i + 1
            
        }
        
        if !trovato {
            neverMissCount = neverMissCount + 1
            if neverMissCount == 10 {
                evaluateNeverMiss()
            }
            touchDown(atPoint: CGPoint(x: 0,y: 0))
            
        }
        else {
            neverMissCount = 0
        }
        
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask) == (PhysicsCategory.lose | PhysicsCategory.gelato) {
            reloadGame()
           
        }
        else if (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask) == (PhysicsCategory.cono | PhysicsCategory.gelato)  {
            
            gelatoDidCollide(nodeA: contact.bodyA.node as! SKSpriteNode, nodeB: contact.bodyB.node as! SKSpriteNode)
        }
        else if (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask) == (PhysicsCategory.gelato | PhysicsCategory.gelato){
            
            gelatoDidCollide(nodeA: contact.bodyA.node as! SKSpriteNode, nodeB: contact.bodyB.node as! SKSpriteNode)
        }
        
    }
    
    var circleLayer = CAShapeLayer()
    
    override func update(_ currentTime: TimeInterval) {

    }
    
    func reloadGame(){
        
        
        if points > UserDefaults.standard.integer(forKey: UDKeys.highScoreKey){
            UserDefaults.standard.set(points, forKey: UDKeys.highScoreKey)
        }
        
        
        self.removeAllChildren()
        self.run(self.woohoo)
        setUpLose()
        setUpLabels()
        
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.posizionaBanner(posizione: CGPoint(x: (view?.frame.midX)!, y: (view?.frame.midY)!))
        gameViewController.addBannerToScene()
        
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -100)
        
        guard let view = self.view else {
            return
        }
        
        myCamera.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        self.camera = myCamera
        
        let waitAction = SKAction.wait(forDuration: 0.01)
        let action = SKAction.run {
            let gelatoRosso: SKSpriteNode = SKSpriteNode(imageNamed: "Regalo")
            gelatoRosso.position = CGPoint(x: CGFloat(Float.random(in: Float(100+view.frame.minX)...Float(view.frame.maxX-100))), y: view.frame.size.height + gelatoRosso.size.height)
            let gelatoSize = CGSize(width: view.frame.width / 10, height: view.frame.width / 10)
            gelatoRosso.physicsBody = SKPhysicsBody(rectangleOf: gelatoSize, center: CGPoint(x: 0, y: 0))
            gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)
            gelatoRosso.physicsBody?.categoryBitMask = PhysicsCategory.gelatoStuck
            gelatoRosso.physicsBody?.contactTestBitMask = PhysicsCategory.lose
            gelatoRosso.physicsBody?.collisionBitMask = PhysicsCategory.all
            gelatoRosso.physicsBody?.affectedByGravity = true
            gelatoRosso.physicsBody?.restitution = 0.0
            self.addChild(gelatoRosso)
        }
        self.run(SKAction.repeat(SKAction.sequence([action, waitAction]), count: 200)) {
            
            var count = 3
            self.menu = SKSpriteNode (imageNamed: "mainMenuButton")
            self.menu.size = CGSize(width: view.frame.width*0.3 , height: view.frame.width*0.15)
            self.menu.position = CGPoint (x: view.frame.midX , y: view.frame.height*0.3)
            self.menu.zPosition = 6
            self.addChild(self.menu)
            
            self.retryNow = SKSpriteNode (imageNamed: "retryNowButton")
            self.retryNow.size = CGSize(width: view.frame.width*0.3 , height: view.frame.width*0.15)
            self.retryNow.position = CGPoint (x: view.frame.midX , y: self.menu.position.y - self.menu.frame.height)
            self.retryNow.zPosition = 6
            self.addChild(self.retryNow)
            

            
            
            var restart = makeTextLabel(text: "Restart in " + String(count), size: view.frame.midX / 5, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.4))
            self.addChild(restart)
            
            
            
            let waitAction = SKAction.wait(forDuration: 1)
            let conteggio = SKAction.run {
                count  = count - 1
                restart.removeFromParent()
                restart = makeTextLabel(text: "Restart in \(count)", size: view.frame.midX / 5, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.4))
                self.addChild(restart)
            }
            
            let reloadGame = SKAction.run {
                
                let scene = GameScene(size: self.size) // Whichever scene you want to restart (and are in)
                let animation = SKTransition.fade(with: UIColor(red: 249/255, green: 201/255, blue: 123/255, alpha: 1.0), duration: 1)
                
                let gameViewController = self.view?.window?.rootViewController as! GameViewController
                gameViewController.removeBannerFromScene()
                isReloaded = true
                self.view?.presentScene(scene, transition: animation)
            }
            
            self.run(SKAction.sequence([waitAction,conteggio,waitAction,conteggio,waitAction,conteggio, waitAction,reloadGame]))
        }
        
    }
    
    func setUpLabels(){
        
        guard let view = self.view else {
            return
        }
        
        let gameOverLabel = makeTextLabel(text: "Game Over", size: view.frame.width/7, position: CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height*0.4))
        
        self.addChild(gameOverLabel)
        
        
        
        
        
    }
    func gelatoDidCollide(nodeA: SKSpriteNode, nodeB: SKSpriteNode){
        //        self.addChild(gnack)
        run(gnak)
        if points == 0 {
            if nodeA.physicsBody?.categoryBitMask == PhysicsCategory.cono{
                nodeA.physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
            else {
                nodeB.physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
        }
        if gelati.count >= 2 {
            
            //            gelati[gelati.count-1].move(toParent: gelati[0])
            for i in 0...gelati.count-1 {
                gelati[i].physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
        }
        guard let view = view else {
            return
        }
        points = points + 1
        
        punteggio.removeFromParent()
        punteggio = makeTextLabel(text: "Score: \(points)", size: view.frame.width/15, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.95))
        var punteggioPosition: CGPoint
        if hasTopNotch {
            punteggioPosition = CGPoint(x: 0, y: view.frame.height/2 - punteggio.frame.height*5/2)
        }
        else {
            punteggioPosition = CGPoint(x: 0, y: view.frame.height/2 - punteggio.frame.height*3/2)
        }
        punteggio.position = punteggioPosition
        
        self.myCamera.addChild(punteggio)
        var gelatoRosso: SKSpriteNode
        
            gelatoRosso = SKSpriteNode(imageNamed: "Gelato"+String(DesignElements.gelatoDesign))
        gelatoRosso.zPosition = CGFloat(points+10)
        
        gelatoRosso.position = CGPoint(x: view.frame.maxX, y: view.frame.height * 0.5 + myCamera.position.y - view.frame.width / 7 * 2)
        gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)

       
        if points >= 4 {
            gelatoRosso.position = CGPoint(x: view.frame.maxX, y: view.frame.height * 0.5 + myCamera.position.y + view.frame.width/8 - gelatoRosso.size.height*2)
            myCamera.run(SKAction.move(to: CGPoint(x: myCamera.position.x, y: myCamera.position.y + gelati[gelati.count-1].size.height - gelati[gelati.count-1].size.height/3), duration: 0.5))

            let actionMoveBackward = SKAction.move(to: CGPoint(x: view.frame.minX+gelatoRosso.size.width, y: gelatoRosso.position.y),
                                                   duration: TimeInterval(timeSpeed))
            let actionMoveForward = SKAction.move(to: CGPoint(x: view.frame.maxX-gelatoRosso.size.width, y: gelatoRosso.position.y), duration: TimeInterval(timeSpeed))
            gelatoRosso.run(SKAction.repeatForever(SKAction.sequence([actionMoveBackward, actionMoveForward])))
            self.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),SKAction.run {
                self.gelati.append(gelatoRosso)
                self.addChild(gelatoRosso)
                self.evaluatePoints()
                }]))
           
        }
        else {
            let actionMoveBackward = SKAction.move(to: CGPoint(x: view.frame.minX+gelatoRosso.size.width, y: gelatoRosso.position.y),
                                                   duration: TimeInterval(timeSpeed))
            let actionMoveForward = SKAction.move(to: CGPoint(x: view.frame.maxX-gelatoRosso.size.width, y: gelatoRosso.position.y), duration: TimeInterval(timeSpeed))
            gelatoRosso.run(SKAction.repeatForever(SKAction.sequence([actionMoveBackward, actionMoveForward])))
            gelati.append(gelatoRosso)
            self.addChild(gelatoRosso)
        }
        
        
        
        touched = false
        timeSpeed = timeSpeed - 0.1
        if timeSpeed <= 0.2 {
            timeSpeed = 0.1
        }
        
    }
    func changeScene(scene: SKScene){
        self.removeAllActions()
        tendina.removeFromParent()
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        let animDown = SKAction.run {
            self.tendaCheScende()
        }
        
        let wait = SKAction.wait(forDuration: 1.5)
        let prensentazione = SKAction.run {
            self.view?.presentScene(scene)
            self.tendina.removeFromParent()
        }
        
        self.run(SKAction.sequence([animDown,wait,prensentazione]))
    }
    
    
}


