//
//  Library.swift
//  Mein Fuhller
//
//  Created by antonio elefante on 05/12/2018.
//  Copyright © 2018 elefante antonio elefante. All rights reserved.
//

import Foundation
import SpriteKit


var isReloaded: Bool = false

struct PhysicsCategory {
    static let none: UInt32 = 0
    static let all: UInt32 = UInt32.max
    static let gelato: UInt32 = 0b1
    static let cono: UInt32 = 0b10
    static let lose: UInt32 = 0b100
    static let gelatoStuck: UInt32 = 0b1000
}



struct ADMob {
    static let appID:String = "ca-app-pub-2335142604931226~5553220604"
    static let bannerID:String = "ca-app-pub-2335142604931226/3860864980"
    static let testBannerID:String = "ca-app-pub-3940256099942544/2934735716"
}

struct DesignElements {
    static var gelatoDesign:Int = 1
    static var tendaDesign:Int = 1
    static var cialdaDesign:Int = 1
    static var tavoloDesign:Int = 1
    static var conoDesign:Int = 1
    static var backgroundColor: UIColor = UIColor(red: 249/255, green: 201/255, blue: 123/255, alpha: 1.0)
}

struct UDKeys {
    static let granellaKey:String = "medagliaGranella"
    static let cannellaKey:String = "medagliaCannella"
    static let impilaKey:String = "medagliaImpila"
    static let cacchinaKey:String = "medagliaCacchina"
    static let collaborazioneKey:String = "medagliaCollaborazione"
    static let carrettinoKey:String = "medagliaCarrettino"
    static let oroKey:String = "medagliaOro"
    static let kingKey:String = "medagliaKing"
    static let finaleKey:String = "medagliaFinale"
    static let highScoreKey:String = "highScoreKey"
    static let gamePlayedKey:String = "gamePlayed"
    
    static let gelatoDesignKey:String = "gelatoDesignKey"
    static let tendaDesignKey:String = "tendaDesignKey"
    static let cialdaDesignKey:String = "cialdaDesignKey"
    static let tavoloDesignKey:String = "tavoloDesignKey"
    static let conoDesignKey:String = "conoDesignKey"
    static let backgroundKey:String = "backgroundKey"
}

struct MedalsLabel {
    static let granellaTitle: String = "Sundae Coin"
    static let granellaMessage: String = "Earned by playing one game"
    
    static let cannellaTitle: String = "Cinnamon Chip"
    static let cannellaMessage: String = "Earned by scoring 5 points"
    
    static let impilaTitle: String = "Triple Decker"
    static let impilaMessage: String = "Earned by playing 10 games"
    
    static let cacchinaTitle: String = "Cream Whip Pin"
    static let cacchinaMessage: String = "Earned by scoring 25 points"
    
    static let collaborazioneTitle: String = "Collaboration Badge"
    static let collaborazioneMessage: String = "Earned by never missing a tap"
    static let collaborazioneMessage2: String = "ten times in a row"
    //    sposta questo come quinto
    static let carrettinoTitle: String = "Ice-Cream Truck"
    
    
    static let carrettinoMessage: String = "Earned by playing 20 games"
    //    sposta questo come quarto
    //    cioè carrettino e collaborazione andrebbero invertiti
    
    static let oroTitle: String = "Golden Gelato"
    static let oroMessage: String = "Earned by scoring 40 points"
    
    static let kingTitle: String = "Ice Cream Crown"
    static let kingMessage: String = "Earned by scoring 75 points"
    
    static let finaleTitle: String = "Team Trophy Syrup"
    static let finaleMessage: String = "Earned by scoring 100 points"
    //    Choose one: "Earned by never missing a tap 15 times in a row" oppure "Earned by scoring 100 points" oppure "Earned by dropping the perfect gelato ball" (mysteriosa ma significa che devi centrare perfettamente ma non glie lo diciamo manco)
}

extension SKLabelNode {
    func addStroke(color:UIColor, width: CGFloat) {
        guard let labelText = self.text else { return }
        let font = UIFont(name: self.fontName!, size: self.fontSize)
        let attributedString:NSMutableAttributedString
        if let labelAttributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        let attributes:[NSAttributedString.Key:Any] = [.strokeColor: color, .strokeWidth: -width, .font: font!, .foregroundColor: self.fontColor!]
        attributedString.addAttributes(attributes, range: NSMakeRange(0, attributedString.length))
        self.attributedText = attributedString
    }
}

func makeTextLabel(text: String, size: CGFloat, position: CGPoint) -> SKLabelNode {
    let label = SKLabelNode(fontNamed: "American Typewriter")
    label.fontColor = .white
    label.fontSize = size
    label.position = position
    label.zPosition = +11
    label.text = text
    label.addStroke(color: .brown, width: 2)
    return label
}

func makeShadowLabel(text: String, size: CGFloat, position: CGPoint) -> SKLabelNode {
    let shadow = SKLabelNode(fontNamed: "American Typewriter")
    shadow.fontColor = .brown
    shadow.fontSize = size
    shadow.position = position
    shadow.zPosition = 10
    shadow.text = text
    return shadow
}

func addLabelToView(node: SKNode, text: String, size:CGFloat, position: CGPoint) {
    let label = makeTextLabel(text: text, size: size, position: position)
    let shadowPosition = CGPoint(x: position.x, y: position.y - 50)
    let shadow = makeShadowLabel(text: text, size: size, position: shadowPosition)
    node.addChild(shadow)
    node.addChild(label)
}

func addLabelToView(node: SKNode, text: String, size:CGFloat, position: CGPoint, numberOfLines: Int) {
    let label = makeTextLabel(text: text, size: size, position: position)
    label.numberOfLines = numberOfLines
    let shadowPosition = CGPoint(x: position.x-5, y: position.y - 5)
    let shadow = makeShadowLabel(text: text, size: size, position: shadowPosition)
    shadow.numberOfLines = numberOfLines
    node.addChild(shadow)
    node.addChild(label)
}
