//
//  GameViewController.swift
//  Mein Fuhller
//
//  Created by antonio elefante on 05/12/2018.
//  Copyright © 2018 antonio elefante. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GoogleMobileAds
class GameViewController: UIViewController, GADBannerViewDelegate{
    
    var adBannerView: GADBannerView!
    func addBannerViewToView(_ bannerView: GADBannerView) {
        guard let view = self.view else {
            return
        }
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        DesignElements.cialdaDesign = UserDefaults.standard.integer(forKey: UDKeys.cialdaDesignKey)
//        DesignElements.conoDesign = UserDefaults.standard.integer(forKey: UDKeys.conoDesignKey)
//        DesignElements.gelatoDesign = UserDefaults.standard.integer(forKey: UDKeys.gelatoDesignKey)
//        DesignElements.tavoloDesign = UserDefaults.standard.integer(forKey: UDKeys.tavoloDesignKey)
//        DesignElements.tendaDesign = UserDefaults.standard.integer(forKey: UDKeys.tendaDesignKey)
        
        DesignElements.cialdaDesign = 2
        DesignElements.conoDesign = 2
        DesignElements.gelatoDesign = 2
        DesignElements.tavoloDesign = 2
        DesignElements.tendaDesign = 2
        DesignElements.backgroundColor = UIColor(red: 35/255, green: 64/255, blue: 2/255, alpha: 1.0)
        
        
        
        let scene = MainMenu(size: view.bounds.size)
        let skView = view as! SKView
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        skView.presentScene(scene)
        
        adBannerView = GADBannerView(adSize: kGADAdSizeBanner)
        adBannerView.layer.position = CGPoint(x: view.frame.midX, y: view.frame.height - 2*kGADAdSizeBanner.size.height)
        
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        adBannerView.adUnitID = ADMob.bannerID

        let reqAd = GADRequest()
        adBannerView.load(reqAd)
        self.view.addSubview(adBannerView)
    }
    
    func removeBannerFromScene(){
        adBannerView.removeFromSuperview()
    }
    func addBannerToScene(){
        let reqAd = GADRequest()
        adBannerView.load(reqAd)
        self.view.addSubview(adBannerView)
    }
    func posizionaBanner(posizione: CGPoint){
        adBannerView.layer.position = posizione
    }
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
