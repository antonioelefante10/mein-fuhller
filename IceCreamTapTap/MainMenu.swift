//
//  MainMenu.swift
//  Mein Fuhller
//
//  Created by alfonso d'auria on 12/12/2018.
//  Copyright © 2018 antonio elefante. All rights reserved.
//


import Foundation
import GameKit
import SpriteKit
import GameplayKit
import GoogleMobileAds

private let background = SKSpriteNode(imageNamed: "sfondo")
private var play = SKSpriteNode(imageNamed: "playButton")
private var music = SKAudioNode(fileNamed: "HollyJolly.mp3")
private var medals = SKSpriteNode (imageNamed: "medalsButton")
private var howToButton = SKSpriteNode(imageNamed: "howToPlayButton")

class MainMenu: SKScene
{
    var tendina = SKSpriteNode (imageNamed: "Tenda2")
    func posizionaTendina(){
        
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: -100)
        tendina.zPosition = 2000
        self.addChild(tendina)
        tendaCheSale()
    }
    func tendaCheSale()
    {
        let up = SKAction.move(to: CGPoint(x: 0, y: (self.view?.frame.height)! ), duration: 1)
        tendina.run(up)
        let wait = SKAction.wait(forDuration: 3)
        self.run(SKAction.sequence([wait,SKAction.run {
            self.tendina.removeFromParent()
            let gameViewController = self.view?.window?.rootViewController as! GameViewController
            gameViewController.posizionaBanner(posizione: CGPoint(x: self.view!.frame.midX, y: self.view!.frame.height - 2*kGADAdSizeBanner.size.height))
            gameViewController.addBannerToScene()
            }]))
    }
    
    
    override func didMove(to view: SKView) {
                posizionaTendina()
        
        background.anchorPoint = CGPoint(x: 0, y: 0)
        background.size =  CGSize(width: view.frame.width, height: view.frame.height)
        background.position = CGPoint(x: 0, y: 0)
        self.backgroundColor = DesignElements.backgroundColor
        setUpLose()
        reloadGame()
        setUpButtons()
        
        
        let titleLabel = makeTextLabel(text: "IceCream", size: view.frame.width/7, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.80))
        
        
        self.addChild(titleLabel)
        
        let tapTapLabel = makeTextLabel(text: "Tap Tap", size: view.frame.width/7, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.80-titleLabel.frame.height))
        
        
        self.addChild(tapTapLabel)
        let highScoreLabel = makeTextLabel(text: "Ice Score: \(UserDefaults.standard.integer(forKey: UDKeys.highScoreKey))", size: view.frame.width/12, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.15))
        self.addChild(highScoreLabel)
        self.addChild(music)
        
    }
    
    func changeScene(scene: SKScene){
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        self.removeAllActions()
        tendina.removeFromParent()
        let animDown = SKAction.run {
            self.tendaCheScende()
        }
        
        let wait = SKAction.wait(forDuration: 1.5)
        let prensentazione = SKAction.run {
            self.view?.presentScene(scene)
            self.tendina.removeFromParent()
        }
        
        self.run(SKAction.sequence([animDown,wait,prensentazione]))
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches
        {
            let location = t.location(in: self)
            
            if play.contains(location)
            {
                
                
                var scene: SKScene
                
                if UserDefaults.standard.bool(forKey: UDKeys.granellaKey) == false && false {
                    scene = HowToScene(size: self.size)
                }
                else {
                    
                    scene = GameScene(size: self.size)
                    
                }
                changeScene(scene: scene)
            }
            
            
            
            if medals.contains(location)
            {
                
                let scene = MedalScene(size: self.size)
                changeScene(scene: scene)
                
            }
            
            
//            if howToButton.contains(location)
//            {
//                let scene = HowToScene(size: self.size)
//                changeScene(scene: scene)
//            }
            
        }
    }
   
    
    func tendaCheScende()
    {
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: self.view!.frame.height + 10)
        tendina.zPosition = 2000
        self.addChild(tendina)
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        let down = SKAction.move(to: CGPoint(x: 0, y: -100 ), duration: 1)
       tendina.run(down)
    }
    
    func setUpButtons(){
        guard let view = self.view else {
            return
        }
        
        medals.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.12)
        medals.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        medals.zPosition = 1
        self.addChild(medals)
        
        
        play.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.12)
        play.position = CGPoint(x: view.frame.midX, y: view.frame.midY + medals.size.height*0.5 + view.frame.height*0.075)
        play.zPosition = 1
        self.addChild(play)
        
        howToButton.size = CGSize(width: view.frame.width*0.3, height: view.frame.width*0.12)
        howToButton.position = CGPoint(x: view.frame.midX, y: view.frame.midY - medals.size.height*0.5 - view.frame.height*0.075)
        howToButton.zPosition = 1
        self.addChild(howToButton)
        
        
        
    }
    
    func reloadGame(){
        
        
        setUpLose()
        
        
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -100)
        
        guard let view = self.view else {
            return
        }
        
        
        
        let waitAction = SKAction.wait(forDuration: 0.01)
        let action = SKAction.run {
            let gelatoRosso: SKSpriteNode = SKSpriteNode(imageNamed: "Regalo")
            gelatoRosso.position = CGPoint(x: CGFloat(Float.random(in: Float(100+view.frame.minX)...Float(view.frame.maxX-100))), y: view.frame.size.height + gelatoRosso.size.height)
            let gelatoSize = CGSize(width: view.frame.width / 10, height: view.frame.width / 10)
            gelatoRosso.physicsBody = SKPhysicsBody(rectangleOf: gelatoSize, center: CGPoint(x: 0, y: 0))
            gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)
            gelatoRosso.physicsBody?.categoryBitMask = PhysicsCategory.gelatoStuck
            gelatoRosso.physicsBody?.contactTestBitMask = PhysicsCategory.lose
            gelatoRosso.physicsBody?.collisionBitMask = PhysicsCategory.all
            gelatoRosso.physicsBody?.affectedByGravity = true
            gelatoRosso.physicsBody?.restitution = 0.0
            self.addChild(gelatoRosso)
        }
        self.run(SKAction.sequence([SKAction.repeat(SKAction.sequence([action, waitAction]), count: 150),SKAction.run {
            self.downWall.removeFromParent()
            self.rightWall.removeFromParent()
            self.leftWall.removeFromParent()
            self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
            }]))
    }
    
    var downWall: SKSpriteNode!
    var rightWall: SKSpriteNode!
    var leftWall: SKSpriteNode!
    func setUpLose(){
        downWall = SKSpriteNode()
        guard let myView = self.view else {
            return
        }
        downWall.color = .red
        downWall.position = CGPoint(x: myView.frame.midX, y: myView.frame.minY-myView.frame.height*0.1)
        downWall.size = CGSize(width: myView.frame.width, height: myView.frame.height*0.1)
        downWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width, height: myView.frame.height*0.1))
        downWall.physicsBody?.isDynamic = false
        downWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        downWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        downWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        rightWall = SKSpriteNode()
        
        rightWall.color = .red
        rightWall.anchorPoint = CGPoint(x: 0, y:0)
        //        rightWall.position = CGPoint(x: myView.frame.width/2+myView.frame.width*0.1, y: -myView.frame.height/2)
        //        rightWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        rightWall.position = CGPoint(x: myView.frame.maxX+myView.frame.width*0.1, y: myView.frame.minY)
        rightWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        rightWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000))
        rightWall.physicsBody?.isDynamic = false
        rightWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        rightWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        rightWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        leftWall = SKSpriteNode()
        
        leftWall.color = .red
        
        leftWall.position = CGPoint(x: myView.frame.minX-myView.frame.width*0.1, y: myView.frame.minY)
        leftWall.size = CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000)
        
        
        leftWall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myView.frame.width*0.1, height: myView.frame.height*1000))
        leftWall.physicsBody?.isDynamic = false
        leftWall.physicsBody?.categoryBitMask = PhysicsCategory.lose
        leftWall.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        leftWall.physicsBody?.collisionBitMask = PhysicsCategory.gelatoStuck
        
        
        self.addChild(downWall)
        self.addChild(rightWall)
        self.addChild(leftWall)
       
    }
    
}
