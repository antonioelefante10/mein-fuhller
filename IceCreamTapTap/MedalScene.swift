//
//  MedalScene.swift
//  Mein Fuhller
//
//  Created by antonio elefante on 11/12/2018.
//  Copyright © 2018 antonio elefante. All rights reserved.
//

import Foundation
import SpriteKit
import GoogleMobileAds

class MedalScene: SKScene {
    var medagliaGranella: SKSpriteNode!
    var medagliaCannella: SKSpriteNode!
    var medagliaImpila: SKSpriteNode!
    var medagliaCacchina: SKSpriteNode!
    var medagliaCollaborazione: SKSpriteNode!
    var medagliaCarrettino: SKSpriteNode!
    var medagliaOro: SKSpriteNode!
    var medagliaKing: SKSpriteNode!
    var medagliaFinale: SKSpriteNode!
    var medalDescription2: SKLabelNode!
    var medalTitle: SKLabelNode!
    var title: SKLabelNode!
    var medalDescription: SKLabelNode!
    var freccia : SKSpriteNode!
    var clicked: Bool = false
    var stitle: SKLabelNode!
    
    
    var tendina = SKSpriteNode (imageNamed: "Tenda2")
    func posizionaTendina(){
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: -100)
        tendina.zPosition = 2000
        self.addChild(tendina)
        tendaCheSale()
    }
    func tendaCheSale()
    {
        let up = SKAction.move(to: CGPoint(x: 0, y: (self.view?.frame.height)! ), duration: 1)
        tendina.run(up)
        let wait = SKAction.wait(forDuration: 3)
        self.run(SKAction.sequence([wait,SKAction.run {
            self.tendina.removeFromParent()
            let gameViewController = self.view?.window?.rootViewController as! GameViewController
            gameViewController.posizionaBanner(posizione: CGPoint(x: self.view!.frame.midX, y: self.view!.frame.height - 2*kGADAdSizeBanner.size.height))
            gameViewController.addBannerToScene()
            }]))
    }
    
    func tendaCheScende()
    {
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: self.view!.frame.height + 10)
        tendina.zPosition = 2000
        self.addChild(tendina)
        let down = SKAction.move(to: CGPoint(x: 0, y: -100 ), duration: 1)
        tendina.run(down)
    }
    func changeScene(scene: SKScene){
        self.removeAllActions()
        tendina.removeFromParent()
        let animDown = SKAction.run {
            self.tendaCheScende()
        }
        
        let wait = SKAction.wait(forDuration: 1.5)
        let prensentazione = SKAction.run {
            self.view?.presentScene(scene)
            self.tendina.removeFromParent()
        }
        
        self.run(SKAction.sequence([animDown,wait,prensentazione]))
    }
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        posizionaTendina()
//        title = SKLabelNode(fontNamed: "American Typewriter")
//        title.color = .white
//        title.fontSize = view.frame.width/10
//        title.position = CGPoint(x: view.frame.midX, y: view.frame.height*0.925)
//        title.zPosition = +2
//        title.text = "Medals Hall"
//        self.addChild(title)
        
        title = makeTextLabel(text: "Medals Hall", size: view.frame.width/10, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.925))
        self.addChild(title)
//        stitle = SKLabelNode(fontNamed: "American Typewriter")
//        stitle.fontColor = .brown
//        stitle.fontSize = view.frame.width/10
//        stitle.position = CGPoint(x: view.frame.midX, y: view.frame.height*0.92)
//        stitle.zPosition = +1
//        stitle.text = "Medals Hall"
//        self.addChild(stitle)
        
        self.backgroundColor = UIColor(red: 249/255, green: 201/255, blue: 123/255, alpha: 1.0)
        
        
        
        
        freccia = SKSpriteNode(imageNamed: "arrow")
        freccia.size = CGSize(width: view.frame.width*0.1, height: view.frame.height*0.05)
        freccia.anchorPoint = CGPoint(x: 0, y: 0)
        freccia.position = CGPoint(x: view.frame.width*0.03, y: view.frame.height*0.925)
        freccia.zPosition = 2
        self.addChild(freccia)
        
        let colatura = SKSpriteNode (imageNamed: "ColaturaMedaglie")
        colatura.size = CGSize (width: view.frame.width , height: view.frame.height*0.3)
        colatura.position = CGPoint(x: view.frame.midX, y: view.frame.maxY-colatura.size.height/2)
        
        colatura.zPosition = -1
        self.addChild(colatura)
        
        
        if UserDefaults.standard.bool(forKey: UDKeys.granellaKey) {
            medagliaGranella = SKSpriteNode (imageNamed: "MedagliaGelato")
        }
        else {
            medagliaGranella = SKSpriteNode (imageNamed: "medagliaCannellaNo")
        }
        
        medagliaGranella.position = CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.4)
        medagliaGranella.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaGranella.zPosition = 0
        self.addChild(medagliaGranella)
        
        if UserDefaults.standard.bool(forKey: UDKeys.cannellaKey) {
            medagliaCannella = SKSpriteNode (imageNamed: "medagliaCannella")
        }
        else {
            medagliaCannella = SKSpriteNode (imageNamed: "medagliaCannellaNo")
        }
        
        medagliaCannella.position = CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.4)
        medagliaCannella.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaCannella.zPosition = 0
        self.addChild(medagliaCannella)
        
        if UserDefaults.standard.bool(forKey: UDKeys.impilaKey) {
            medagliaImpila = SKSpriteNode (imageNamed: "medagliaImpila")
        }
        else {
            medagliaImpila = SKSpriteNode (imageNamed: "medagliaCannellaNo")
        }
        medagliaImpila.position = CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.4)
        medagliaImpila.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaImpila.zPosition = 0
        self.addChild(medagliaImpila)
        
        if UserDefaults.standard.bool(forKey: UDKeys.cacchinaKey) {
            medagliaCacchina = SKSpriteNode (imageNamed: "medagliaCacchina")
        }
        else {
            medagliaCacchina = SKSpriteNode (imageNamed: "medagliaCacchinaNo")
        }
        medagliaCacchina.position = CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.6)
        medagliaCacchina.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaCacchina.zPosition = 0
        self.addChild(medagliaCacchina)
        
        
        if UserDefaults.standard.bool(forKey: UDKeys.collaborazioneKey) {
            medagliaCollaborazione = SKSpriteNode (imageNamed: "medagliaCollaborazione")
        }
        else {
            medagliaCollaborazione = SKSpriteNode (imageNamed: "medagliaCollaborazioneNo")
        }
        medagliaCollaborazione.position = CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.6)
        medagliaCollaborazione.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaCollaborazione.zPosition = 0
        self.addChild(medagliaCollaborazione)
        
        
        if UserDefaults.standard.bool(forKey: UDKeys.carrettinoKey) {
            medagliaCarrettino = SKSpriteNode (imageNamed: "medagliaCarrettino")
        }
        else {
            medagliaCarrettino = SKSpriteNode (imageNamed: "medagliaCarrettinoNo")
        }
        medagliaCarrettino.position = CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.6)
        medagliaCarrettino.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaCarrettino.zPosition = 0
        self.addChild(medagliaCarrettino)
        
        if UserDefaults.standard.bool(forKey: UDKeys.oroKey) {
            medagliaOro = SKSpriteNode (imageNamed: "medagliaOro")
        }
        else {
            medagliaOro = SKSpriteNode (imageNamed: "medagliaOroNo")
        }
        medagliaOro.position = CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.8)
        medagliaOro.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaOro.zPosition = 0
        self.addChild(medagliaOro)
        
        
        if UserDefaults.standard.bool(forKey: UDKeys.kingKey) {
            medagliaKing = SKSpriteNode (imageNamed: "medagliaKing")
        }
        else {
            medagliaKing = SKSpriteNode (imageNamed: "medagliaKingNo")
        }
        medagliaKing.position = CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.8)
        medagliaKing.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaKing.zPosition = 0
        self.addChild(medagliaKing)
        
        
        if UserDefaults.standard.bool(forKey: UDKeys.finaleKey) {
            medagliaFinale = SKSpriteNode (imageNamed: "medagliaFinale")
        }
        else {
            medagliaFinale = SKSpriteNode (imageNamed: "medagliaFinaleNo")
        }
        medagliaFinale.position = CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.8)
        medagliaFinale.size = CGSize (width: view.frame.width*0.2 , height: view.frame.width*0.2)
        medagliaFinale.zPosition = 0
        self.addChild(medagliaFinale)
        
        medagliaGranella.name = "medagliaGranella"
        medagliaCannella.name = "medagliaCannella"
        medagliaImpila.name = "medagliaImpila"
        medagliaCacchina.name = "medagliaCacchina"
        medagliaCollaborazione.name = "medagliaCollaborazione"
        medagliaCarrettino.name = "medagliaCarrettino"
        medagliaOro.name = "medagliaOro"
        medagliaKing.name = "medagliaKing"
        medagliaFinale.name = "medagliaFinale"
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.location(in: self)
            if medagliaCannella.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaCannella, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaGranella.contains(location) {
               
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaGranella, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaImpila.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaImpila, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaCacchina.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaCacchina, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaCollaborazione.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaCollaborazione, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaCarrettino.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaCarrettino, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaOro.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaOro, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaKing.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaKing, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if medagliaFinale.contains(location) {
                
                if !clicked {
                    clicked = true
                    funzioneFrisiufh(except: medagliaFinale, duration: 0.5)
                }
                else {
                    reloadSpritePosition(duration: 0.5)
                    clicked = false
                }
                
            }
            else if freccia.contains(location) {
                let scene = MainMenu(size: self.size)
                changeScene(scene: scene)
            }
            else {
                
                reloadSpritePosition(duration: 0.5)
                clicked = false
            }
        }
    }
    
    func funzioneFrisiufh(except: SKSpriteNode, duration: Double) {
        
        guard let view = self.view else {
            return
        }
        
        
        switch except.name {
        case medagliaGranella.name:
            medalTitle = makeTextLabel(text: MedalsLabel.granellaTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.granellaMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaCannella.name:
            medalTitle = makeTextLabel(text: MedalsLabel.cannellaTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.cannellaMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaOro.name:
            medalTitle = makeTextLabel(text: MedalsLabel.oroTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.oroMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
        case medagliaImpila.name:
            
            medalTitle = makeTextLabel(text: MedalsLabel.impilaTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.impilaMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaCacchina.name:
            
            medalTitle = makeTextLabel(text: MedalsLabel.cacchinaTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.cacchinaMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaCollaborazione.name:
            medalTitle = makeTextLabel(text: MedalsLabel.collaborazioneTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.collaborazioneMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            
            medalDescription2 = makeTextLabel(text: MedalsLabel.collaborazioneMessage2, size: view.frame.width*0.05, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35 - medalDescription.frame.height))
            
            self.addChild(medalDescription2)
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaCarrettino.name:
            medalTitle = makeTextLabel(text: MedalsLabel.carrettinoTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.carrettinoMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaKing.name:
            medalTitle = makeTextLabel(text: MedalsLabel.kingTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.kingMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaFinale.size.width, y: view.frame.minY-medagliaFinale.size.height), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        case medagliaFinale.name:
            medalTitle = makeTextLabel(text: MedalsLabel.finaleTitle, size: view.frame.width*0.1, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.25))
            medalDescription = makeTextLabel(text: MedalsLabel.finaleMessage, size: view.frame.width*0.055, position: CGPoint(x: view.frame.midX, y: view.frame.height*0.5 - view.frame.width*0.35))
            
            medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaGranella.size.width, y: view.frame.maxY + medagliaGranella.size.height), duration: duration))
            medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.maxY + medagliaCannella.size.height), duration: duration))
            medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.maxX + medagliaImpila.size.width, y: view.frame.maxY + medagliaImpila.size.height), duration: duration))
            medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaCacchina.size.width, y: view.frame.midY), duration: duration))
            medagliaCollaborazione.run(SKAction.scale(to: 0, duration: duration))
            medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.maxX+medagliaCarrettino.size.width, y: view.frame.midY), duration: duration))
            medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.minY-medagliaKing.size.height), duration: duration))
            medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.midY), duration: duration))
            medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.minX - medagliaOro.size.width, y: view.frame.minY - medagliaOro.size.height), duration: duration))
        default:
            break
        }
        self.addChild(medalDescription)
        self.addChild(medalTitle)
    }
    
    func reloadSpritePosition(duration: Double){
        guard let view = self.view else {
            return
        }
        if medalDescription != nil  {
            medalDescription.removeFromParent()
            
        }
        if  medalTitle != nil {
            medalTitle.removeFromParent()
        }
        if medalDescription2 != nil {
            medalDescription2.removeFromParent()
        }
        
        medagliaGranella.run(SKAction.move(to: CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.4), duration: duration))
        
        medagliaCannella.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.4), duration: duration))
        
        
        medagliaImpila.run(SKAction.move(to: CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.4), duration: duration))
        
        
        medagliaCacchina.run(SKAction.move(to: CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.6), duration: duration))
        
        medagliaCollaborazione.run(SKAction.move(to:  CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.6), duration: duration))
        
        medagliaCarrettino.run(SKAction.move(to: CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.6), duration: duration))
        
        medagliaOro.run(SKAction.move(to: CGPoint(x: view.frame.midX - view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.8), duration: duration))
        medagliaCollaborazione.run(SKAction.scale(to: 1, duration: duration))
        medagliaKing.run(SKAction.move(to: CGPoint(x: view.frame.midX, y: view.frame.height - view.frame.height * 0.8), duration: duration))
        
        medagliaFinale.run(SKAction.move(to: CGPoint(x: view.frame.midX + view.frame.width * 0.3, y: view.frame.height - view.frame.height * 0.8), duration: duration))
        
    }
}
