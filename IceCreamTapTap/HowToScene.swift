//
//  GameScene.swift
//  Mein Fuhller
//
//  Created by antonio elefante on 05/12/2018.
//  Copyright © 2018 antonio elefante. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit

class HowToScene: SKScene , SKPhysicsContactDelegate, UIGestureRecognizerDelegate{
    
    private var points = 0
    private var conoGelato: SKSpriteNode = SKSpriteNode(imageNamed: "cono")
    private var touched: Bool = false
    private var gelati: [SKSpriteNode] = []
    var gnak = SKAction.playSoundFileNamed("squirt.wav", waitForCompletion: false)
    private var arrayOfTouches: [CGPoint] = []
    private let musictheme = SKAudioNode (fileNamed: "musicgame.mp3")
    private var cialda1: SKSpriteNode!
    private var cialda2: SKSpriteNode!
    private var cialda3: SKSpriteNode!
    private var cialda4: SKSpriteNode!
    private var table: SKSpriteNode!
    private var successCount = 0
    private var message: SKLabelNode!
    
    var tendina = SKSpriteNode (imageNamed: "Tenda2")
    func posizionaTendina(){
        let gameViewController = self.view?.window?.rootViewController as! GameViewController
        gameViewController.removeBannerFromScene()
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: -100)
        tendina.zPosition = 2000
        self.addChild(tendina)
        
        tendaCheSale()
    }
    func tendaCheSale()
    {
        let up = SKAction.move(to: CGPoint(x: 0, y: (self.view?.frame.height)! ), duration: 1)
        tendina.run(up)
        let wait = SKAction.wait(forDuration: 3)
        self.run(SKAction.sequence([wait,SKAction.run {
            self.tendina.removeFromParent()
            }]))
    }
    func tendaCheScende()
    {
        
        tendina.anchorPoint = CGPoint(x: 0, y: 0)
        tendina.size = CGSize(width: self.view!.frame.width , height: self.view!.frame.height + 100)
        tendina.position = CGPoint(x: 0, y: self.view!.frame.height + 10)
        tendina.zPosition = 2000
        self.addChild(tendina)
        
        let down = SKAction.move(to: CGPoint(x: 0, y: -100 ), duration: 1)
        tendina.run(down)
    }
    func changeScene(scene: SKScene){
        self.removeAllActions()
        tendina.removeFromParent()
        let animDown = SKAction.run {
            self.tendaCheScende()
        }
        
        let wait = SKAction.wait(forDuration: 1.5)
        let prensentazione = SKAction.run {
            self.view?.presentScene(scene)
            self.tendina.removeFromParent()
        }
        
        self.run(SKAction.sequence([animDown,wait,prensentazione]))
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    var action: SKAction!
    var circleLayer = CAShapeLayer()
    var touchLocation: CGPoint!
    @objc func longPress(sender: UILongPressGestureRecognizer) {
        
        if sender.state == .began {
            
            
                circleLayer.removeAllAnimations()
                circleLayer.removeFromSuperlayer()
            
            touchLocation = sender.location(in: self.view)
            let node = self.atPoint(touchLocation)
            if node.name == "1" || node.name == "2" || node.name == "3" || node.name == "4" {
                return
            }
            addCircleView(position: touchLocation)
            action = SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run {
                self.circleLayer.removeFromSuperlayer()
                self.circleLayer.removeAllAnimations()
                let scene = MainMenu(size: self.size)
                self.changeScene(scene: scene)
                }])
            self.run(action, withKey: "pauseAction")
            //            startTime = shape1.convertTime(CACurrentMediaTime(), from: nil)
            
        }
        if sender.state == .ended {
            
                circleLayer.removeFromSuperlayer()
                circleLayer.removeAllAnimations()
            
            
            
            
            self.removeAction(forKey: "pauseAction")
            
        }
        
        
    }
    func addCircleView(position: CGPoint) {
        guard let view = self.view else {
            return
        }
        let circlePath = UIBezierPath(arcCenter: position, radius: (frame.size.width)/10, startAngle: 0.0, endAngle: CGFloat(.pi * 2.0), clockwise: true)
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = UIColor.brown.cgColor
        circleLayer.lineWidth = 5.0;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        // Add the circleLayer to the view's layer's sublayers
        view.layer.addSublayer(circleLayer)
        
        
        animateCircle(duration: 3)
        view.layer.addSublayer(circleLayer)
    }
    
    func animateCircle(duration: TimeInterval) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    override func didMove(to view: SKView) {
        
        let pressed:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(sender:)))
        pressed.delegate = self
        pressed.minimumPressDuration = 0.5
        view.addGestureRecognizer(pressed)
       
        posizionaTendina()
        self.backgroundColor = UIColor(red: 249/255, green: 201/255, blue: 123/255, alpha: 1.0)
        self.addChild(musictheme)
        
        table = SKSpriteNode (imageNamed: "Tavolo1")
        table.anchorPoint = CGPoint(x: 0, y: 0)
        table.position = CGPoint(x: 0, y: -view.frame.height*0.1)
        table.size = CGSize (width: view.frame.width , height: view.frame.height*0.2)
        table.zPosition = -2
        self.addChild(table)
        
        cialda1 = SKSpriteNode(imageNamed: "buttongame1")
        cialda1.name = "1"
        cialda1.size = CGSize(width: view.frame.width*0.2, height: view.frame.width*0.2)
        cialda1.position = CGPoint(x: view.frame.minX+cialda1.size.width/2, y: view.frame.minY+cialda1.size.height/2)
        addChild(cialda1)
        
        
        cialda2 = SKSpriteNode(imageNamed: "buttongame2")
        cialda2.name = "2"
        cialda2.size = CGSize(width: view.frame.width*0.2, height: view.frame.width*0.2)
        cialda2.position = CGPoint (x: view.frame.minX+cialda2.size.width/2, y: view.frame.maxY-cialda2.size.height/2)
        addChild(cialda2)
        
        
        cialda3 = SKSpriteNode(imageNamed: "buttongame3")
        cialda3.name = "3"
        
        cialda3.size = CGSize(width: view.frame.width*0.2, height: view.frame.width*0.2)
        cialda3.position = CGPoint (x: view.frame.maxX-cialda3.size.width/2, y: view.frame.maxY-cialda3.size.height/2)
        addChild(cialda3)
        
        
        cialda4 = SKSpriteNode(imageNamed: "buttongame4")
        cialda4.name = "4"
        
        cialda4.size = CGSize(width: view.frame.width*0.2, height: view.frame.width*0.2)
        cialda4.position = CGPoint (x: view.frame.maxX-cialda4.size.width/2, y: view.frame.minY+cialda4.size.height/2)
        addChild(cialda4)
        
        
        tukituki()
        
        
        var gelatoRosso: SKSpriteNode
        gelatoRosso = SKSpriteNode(imageNamed: "Gelato1")
        gelatoRosso.position = CGPoint(x: view.frame.maxX, y: view.frame.height * 0.5 - view.frame.width / 7 * 2)
        gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)
        self.addChild(gelatoRosso)
        
        conoGelato.position = CGPoint(x: view.frame.midX, y: view.frame.height*0.08)
        conoGelato.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 5)
        let conoGelatoSize = CGSize(width: conoGelato.size.width, height: conoGelato.size.height)
        conoGelato.physicsBody = SKPhysicsBody(rectangleOf: conoGelatoSize, center: CGPoint(x: 0, y: -conoGelato.size.height/3))
        conoGelato.physicsBody?.categoryBitMask = PhysicsCategory.cono
        conoGelato.physicsBody?.isDynamic = false
        conoGelato.physicsBody?.friction = 1
        conoGelato.physicsBody?.contactTestBitMask = PhysicsCategory.gelato
        conoGelato.physicsBody?.collisionBitMask = PhysicsCategory.gelato
        conoGelato.physicsBody?.affectedByGravity = false
        conoGelato.zPosition = -1
        conoGelato.physicsBody?.restitution = 0.0
        
        
        
        self.addChild(conoGelato)
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -3)
        self.physicsWorld.contactDelegate = self
        
        
        
    }
    
    
    
    func tukituki(){
        
        guard let view = self.view else {
            return
        }
        
        
        
        let howToPlaySource = SKSpriteNode(imageNamed: "howToIpad")
        howToPlaySource.zPosition = -2
        howToPlaySource.size = CGSize(width: view.frame.width*9/16, height: view.frame.height*16/9*0.3)
        howToPlaySource.position = CGPoint(x: view.frame.midX, y: view.frame.midY)
        self.addChild(howToPlaySource)
        
        let rightBottomHand = SKSpriteNode(imageNamed: "fingerBottomRight")
        rightBottomHand.zPosition = -1
        rightBottomHand.size = CGSize(width: view.frame.width*0.2, height: view.frame.height*0.2)
        rightBottomHand.position = CGPoint(x: view.frame.midX + howToPlaySource.size.width/2 + rightBottomHand.size.width/4, y: view.frame.midY - howToPlaySource.size.height/2 - rightBottomHand.size.height/4)
        
        
        let pathRBH = CGMutablePath()
        pathRBH.move(to: rightBottomHand.position)
        pathRBH.addLine(to: CGPoint(x: rightBottomHand.position.x - rightBottomHand.size.width * 0.4, y: rightBottomHand.position.y + rightBottomHand.size.height * 0.4))
        
        let actionRBH = SKAction.follow(pathRBH, asOffset: false, orientToPath: false, speed: 250)
        let actionReverseRBH = SKAction.reversed(actionRBH)
        
        rightBottomHand.run(SKAction.repeatForever(SKAction.sequence([actionRBH, actionReverseRBH()])))
        self.addChild(rightBottomHand)
        
        let rightTopHand = SKSpriteNode(imageNamed: "fingerTopRight")
        rightTopHand.zPosition = -1
        rightTopHand.size = CGSize(width: view.frame.width*0.2, height: view.frame.height*0.2)
        rightTopHand.position = CGPoint(x: view.frame.midX + howToPlaySource.size.width/2 + rightTopHand.size.width/4, y: view.frame.midY + howToPlaySource.size.height/2 + rightTopHand.size.height/4)
        
        let pathRTH = CGMutablePath()
        pathRTH.move(to: rightTopHand.position)
        pathRTH.addLine(to: CGPoint(x: rightTopHand.position.x - rightTopHand.size.width * 0.4, y: rightTopHand.position.y - rightTopHand.size.height * 0.4))
        
        let actionRTH = SKAction.follow(pathRTH, asOffset: false, orientToPath: false, speed: 250)
        let actionReverseRTH = SKAction.reversed(actionRTH)
        
        rightTopHand.run(SKAction.repeatForever(SKAction.sequence([actionRTH, actionReverseRTH()])))
        self.addChild(rightTopHand)
        
        let leftTopHand = SKSpriteNode(imageNamed: "fingerTopLeft")
        leftTopHand.zPosition = -1
        leftTopHand.size = CGSize(width: view.frame.width*0.2, height: view.frame.height*0.2)
        leftTopHand.position = CGPoint(x: view.frame.midX - howToPlaySource.size.width/2 - leftTopHand.size.width/4, y: view.frame.midY + howToPlaySource.size.height/2 + leftTopHand.size.height/4)
        
        let pathLTH = CGMutablePath()
        pathLTH.move(to: leftTopHand.position)
        pathLTH.addLine(to: CGPoint(x: leftTopHand.position.x + leftTopHand.size.width * 0.4, y: leftTopHand.position.y - leftTopHand.size.height * 0.4))
        
        let actionLTH = SKAction.follow(pathLTH, asOffset: false, orientToPath: false, speed: 250)
        let actionReverseLTH = SKAction.reversed(actionLTH)
        
        leftTopHand.run(SKAction.repeatForever(SKAction.sequence([actionLTH, actionReverseLTH()])))
        self.addChild(leftTopHand)
        
        let leftBottomHand = SKSpriteNode(imageNamed: "fingerBottomLeft")
        leftBottomHand.zPosition = -1
        leftBottomHand.size = CGSize(width: view.frame.width*0.2, height: view.frame.height*0.2)
        leftBottomHand.position = CGPoint(x: view.frame.midX - howToPlaySource.size.width/2 - leftBottomHand.size.width/4, y: view.frame.midY - howToPlaySource.size.height/2 - leftBottomHand.size.height/4)
        
        let pathLBH = CGMutablePath()
        pathLBH.move(to: leftBottomHand.position)
        pathLBH.addLine(to: CGPoint(x: leftBottomHand.position.x + leftBottomHand.size.width * 0.4, y: leftBottomHand.position.y + leftBottomHand.size.height * 0.4))
        
        let actionLBH = SKAction.follow(pathLBH, asOffset: false, orientToPath: false, speed: 250)
        let actionReverseLBH = SKAction.reversed(actionLBH)
        
        leftBottomHand.run(SKAction.repeatForever(SKAction.sequence([actionLBH, actionReverseLBH()])))
        self.addChild(leftBottomHand)
        
        
        let message = SKLabelNode(fontNamed: "American Typewriter")
        message.fontColor = .black
        message.fontSize = view.frame.width*0.045
        message.position = CGPoint(x: view.frame.midX, y: howToPlaySource.frame.midY - howToPlaySource.size.height*0.255)
        message.zPosition = -1
        message.text = "Tap On The Four \nCorners To Drop\nThe IceCream Balls\nOn The Cone!\n \nTry To Collaborate\nAnd Gain 3 Points!"
        
        
        message.numberOfLines = 7
        self.addChild(message)
        
        self.run(SKAction.sequence([SKAction.wait(forDuration: 10), SKAction.run({
            howToPlaySource.run(SKAction.fadeOut(withDuration: 1))
            rightBottomHand.run(SKAction.fadeOut(withDuration: 1))
            leftBottomHand.run(SKAction.fadeOut(withDuration: 1))
            rightTopHand.run(SKAction.fadeOut(withDuration: 1))
            leftTopHand.run(SKAction.fadeOut(withDuration: 1))
            message.run(SKAction.fadeOut(withDuration: 1))
        })]))
        
    }
    
    
    
    
    
    
    func touchDown(atPoint pos : CGPoint) {
        if !touched {
            
            touched = true
            gelati[gelati.count-1].removeAllActions()
            let gelatoSize = CGSize(width: gelati[gelati.count-1].size.width, height: gelati[gelati.count-1].size.height - gelati[gelati.count-1].size.height/6)
            gelati[gelati.count-1].physicsBody = SKPhysicsBody(rectangleOf: gelatoSize, center: CGPoint(x: 0, y: -gelati[gelati.count-1].size.height/12))
            gelati[gelati.count-1].physicsBody?.isDynamic = true
            gelati[gelati.count-1].physicsBody?.friction = 1
            gelati[gelati.count-1].physicsBody?.allowsRotation = true
            gelati[gelati.count-1].physicsBody?.categoryBitMask = PhysicsCategory.gelato
            gelati[gelati.count-1].physicsBody?.contactTestBitMask = PhysicsCategory.gelato | PhysicsCategory.cono
            gelati[gelati.count-1].physicsBody?.collisionBitMask = PhysicsCategory.all
            gelati[gelati.count-1].physicsBody?.affectedByGravity = true
            gelati[gelati.count-1].physicsBody?.restitution = 0.0
        }
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            
            let location = t.location(in: self)
            
            
            arrayOfTouches.append(t.location(in: view))
        }
        if arrayOfTouches.count >= 4 {
            funzioneMagicaCheFaTocchiNegliAngoli()
        }
        
        
        let wait = SKAction.wait(forDuration: 0.35)
        
        
        self.run(wait) {
            self.arrayOfTouches.removeAll()
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
    }
    
    func funzioneMagicaCheFaTocchiNegliAngoli(){
        var arrayNode: [SKNode] = []
        for i in 0...3 {
            let node: SKNode = self.atPoint(arrayOfTouches[i])
            arrayNode.append(node)
        }
        var trovato = false
        var i = 0
        while !trovato && i<=2 {
            for j in i+1...3 {
                if arrayNode[i].name == arrayNode[j].name {
                    trovato = true
                }
            }
            i = i + 1
            
        }
        
        if !trovato {
            
            
            
            touchDown(atPoint: CGPoint(x: 0,y: 0))
        }
        else {
            
        }
        
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask) == (PhysicsCategory.cono | PhysicsCategory.gelato)  {
            gelatoDidCollide(nodeA: contact.bodyA.node as! SKSpriteNode, nodeB: contact.bodyB.node as! SKSpriteNode)
        }
        else if (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask) == (PhysicsCategory.gelato | PhysicsCategory.gelato){
            
            gelatoDidCollide(nodeA: contact.bodyA.node as! SKSpriteNode, nodeB: contact.bodyB.node as! SKSpriteNode)
        }
        
    }
    
    
    
    
    func gelatoDidCollide(nodeA: SKSpriteNode, nodeB: SKSpriteNode){
        //        self.addChild(gnack)
        run(gnak)
        if points == 0 {
            if nodeA.physicsBody?.categoryBitMask == PhysicsCategory.cono{
                nodeA.physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
            else {
                nodeB.physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
        }
        if gelati.count >= 2 {
            
            //            gelati[gelati.count-1].move(toParent: gelati[0])
            for i in 0...gelati.count-1 {
                gelati[i].physicsBody?.contactTestBitMask = PhysicsCategory.none
            }
        }
        guard let view = view else {
            return
        }
        points = points + 1
        
        var gelatoRosso: SKSpriteNode
        if /*Int.random(in: 0...100) < 50*/ true {
            gelatoRosso = SKSpriteNode(imageNamed: "gelatoRosso")
            
        }
        else {
            gelatoRosso = SKSpriteNode(imageNamed: "gelatoscuro")
        }
        
        gelatoRosso.size = CGSize(width: view.frame.width / 6, height: view.frame.width / 7)
        
        if hasTopNotch {
            gelatoRosso.position = CGPoint(x: view.frame.midX, y: view.frame.height - gelatoRosso.size.height/2 - gelatoRosso.size.height * 0.4)
        }
        else {
            gelatoRosso.position = CGPoint(x: view.frame.midX, y: view.frame.height - gelatoRosso.size.height/2 - gelatoRosso.size.height * 0.1)
        }
        gelatoRosso.zPosition = CGFloat(points)
        touched = false
        
        successCount = successCount + 1
        if successCount == 3 {
            
            let scene = GameScene(size: self.size)
            changeScene(scene: scene)
        }
        
        gelati.append(gelatoRosso)
        self.addChild(gelatoRosso)
    }
}


